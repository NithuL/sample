package outh

import (
	"github.com/intelops/go-common/logging"
	cerbosclient "gitlab.com/tariandev_intelops/new/sample/cerbose"
	"gitlab.com/tariandev_intelops/new/sample/config"
	oryclient "gitlab.com/tariandev_intelops/new/sample/ory"
	"gitlab.com/tariandev_intelops/new/sample/ports"
	"google.golang.org/grpc"
	"gorm.io/gorm"
)

type Adapter struct {
	log          logging.Logger
	db           *gorm.DB
	grpcServer   *grpc.Server
	cfg          *config.Configurtion
	oryClient    oryclient.OryClient
	userapi      ports.UserAPIPort
	cerbosClient *cerbosclient.Client
}
